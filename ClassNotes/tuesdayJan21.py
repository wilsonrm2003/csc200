#classes are a type of callable 
#class Point:# teaching python a new trick
#    pass
#p = Point() #creates an instance of a class
#p2 = Point #makes an alias for point which means it equals p
#p3 = p2() #is also an instance of a point
# attributes --> important vocab word 
# name for functions --> methods
class Point:

    def __init__(self, x=0, y=0):# the names dont need to be this but youll confuse ppl if you change them
        self.x = x
        self.y = y

    def distance(self, other):
        return ((self.x - other.x) ** 2 + (self.y - other.y) ** 2) ** 0.5
