fibs = {0:1, 1:1 }
def fib(n):
    if n in fibs:
        return fibs[n]
    return fib(n - 1) + fib(n - 2)
