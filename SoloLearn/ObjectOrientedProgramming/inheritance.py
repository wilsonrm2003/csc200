"""
inheritance shares functionality between classes 
for example there are classes Dog, Cat, Rabbit, etc. which differ in many ways 
but in some they are similar like they all have a color and a name 
their similarity can be shared by creating a superclass called Animal which only contains the parameters name and color
to inherit a superclass when defining the class put the superclass' name in
parentheses
"""
class Animal:
  def __init__(self, name, color):
    self.name = name
    self.color = color
class Cat(Animal): # cat is calling the superclass animal
  def purr(self):
    print("Purr...")
class Wolf:
    def __init__(self, name, color):
    self.name = name
    self.color = color
  def bark(self):
    print("Grr...")
class Dog(Wolf):
# the class that inherits from the supercalls is called a subclass 
  def bark(self):
    print("Woof!")#a class that inherits the same attributes overrides them
fido = Dog("Fido", "brown")
print(fido.color)
fido.bark()
"""
a class can be inherited indirectly 
One class inherits from another and that class is inherited by a third class 
"""
class A:
  def method(self):
    print("A method")
class B(A):
  def another_method(self):
    print("B method")
class C(B):
  def third_method(self):
    print("C method")
c = C()
c.method()
c.another_method()
c.third_method()
"""
the super function is useful for inheritance that is working with a parent class it is used to find a method with a name in the objects superclass
"""
class AB:
  def spam(self):
    print(1)
class BA(AB):
  def spam(self):
    print(2)
    super().spam()
BA().spam()

