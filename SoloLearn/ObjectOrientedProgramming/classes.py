"""
we've seen two types of programming imparative and functional the thrid one
we're looking at is object oriented programming 
Objects are created using classes, which are the focus point of OOP. 
a class is an objects blueprint
you can use one class to create multiple objects
"""
class classRoom:# classes are created using keyword class and intented blocks
    def __init__(self, room, number): # the intented blocks have methods inside
        self.room = room
        self.number = number
computerscience = classRoom("trailers", "508")
#question 1:  a method is a type of function
"""
__init__ is THE most important method of class :)
it is called when an object is created using the blueprint class created
all methods MUST have self as first perameter as to not confuse people 
"""
computerscience = classRoom("trailers", "508")#this statement creates an instance of computerscience at trailers 508
#question 2: 
class Student:
    def __init__(self, name):
        self.name = name
test = Student("Bob")
"""
methods are added to classes to give function to the class 
*remember all methods must have class as first parameter
"""
#question 3 
class Studant:
    def __init__(self, name):
        self.name = name
    def sayHi(self):
        print("Hi from "+ self.name)
s1 = Studant("Amy")
s1.sayHi()
"""
trying to access an instance of an attribute causes an AttributeError 
which also comes up when you call an undefined method 
"""
