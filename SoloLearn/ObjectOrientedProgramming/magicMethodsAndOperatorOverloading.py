"""
special methods that have double-underscores(dunders) at the beginning and end
of their names for now in solo learn we only saw them as __init__ but there are many other magic methods 
magic methods are actually used to create functionality that cant be in normal 
methods 
A common use of Magic methods are operator overloading, which is defining 
operators like + and * to be used
"""
class Vector2D:
  def __init__(self, x, y):
    self.x = x
    self.y = y
  def __add__(self, other):
    return Vector2D(self.x + other.x, self.y + other.y)
first = Vector2D(5, 7)
second = Vector2D(3, 9)
result = first + second
print(result.x)
print(result.y)
# question 1: magic method for creating instance? __init__
"""
common operator magic methods:
    __sub__ for -
    __mul__ for *
    __truediv__ for /
    __floordiv__ for //
    __mod__ for %
    __pow__ for **
    __and__ for &
    __xor__ for ^
    __or__ for |
"""
#question 2: what is A() ^ B() equal to if A doesnt use magic methods?
# B().__rxor__(A())
"""
common comparing magic methods:
    __lt__ for <
    __le__ for <=
    __eq__ for ==
    __ne__ for !=
    __gt__ for >
    __ge__ for >=
"""
# question 3: what is __le__ magic method for?
# x <= y 
"""
common container magic methods:
    __len__ for len()
    __getitem__ for indexing
    __setitem__ for assigning to indexed values
    __delitem__ for deleting indexed values
    __iter__ for iteration over objects (e.g., in for loops)
    __contains__ for in
"""
#question 4: which magic method call is made by x[y] = z 
# x.setitem(y, z) 
