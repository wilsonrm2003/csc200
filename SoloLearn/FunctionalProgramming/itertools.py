from itertools import *
# a module that contains useful programming functions
# has infinite iterator functions
# the count function counts infinitly from a number
for i in count(15):
    print(i)
    if i >= 19:#without this if statment the code would go on forever
        break
# the cycle function infinitly goes through an iterable ex. Strings
#the repeat function repeats infinitly or a specific number of times
# takewhile function takes items from an iterable while something is true
#chain function combines several iterables into one long one
# accumulate function returns a running total of values into an iterable
#other functions are product and permutation, used to acomplish a task
