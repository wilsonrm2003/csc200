# recursion of factorial
def factorial(x):
    if x == 1:
        return 1 
    else:
        return x * factorial(x-1)
#this recursion function runs an infinite number of times and has a runtime error
def forever(x):
    return x * forever(x-1)
print(factorial(5))
# print(forever(5))# if you uncomment this it becomes a runtime error
#indirect recursion can occur by two functions calling eachother
def is_even(x):
    if x == 0:
        return True
    else:
        return is_odd(x-1)
def is_odd(x):
    return not is_even(x)
print(is_odd(17))
print(is_even(23))
#practice question 
def fib(x):
  if x == 0 or x == 1:
    return 1
  else:
    return fib(x-1) + fib(x-2)
print(fib(4))
