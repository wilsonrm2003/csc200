#sets are data structures similar to lists and dictionaries
#sets are unorganized and cannot be indexed
resturant_set = set({"Shake Shack","Medium Rare","Chipotle","Starbucks"})
num_set = {5, 16, 20}
print(17 in num_set)
print("Chipotle" not in resturant_set)
#use add instead of append to add to sets 
num_set.add(24)
num_set.remove(20)
print(num_set)
#sets can be combined mathmatically
num_set2 = {16, 24, 18}
print(num_set | num_set2) #combines sets to form one containing items in either
print(num_set & num_set2) #gets only items in both
print(num_set - num_set2) #gets items in the first set but not the second
print(num_set ^ num_set2) #gets items in either set but not both
