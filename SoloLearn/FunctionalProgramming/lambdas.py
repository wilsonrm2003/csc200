#lambdas creates a function and imedietly assigns a var to it
#can be created on the fly without assinging a variable
#they arent as powerful as functions because they only do things that require a single expression
def notlambda(x):
    return x ** 2 + 5 * x + 4 
print(notlambda)
print((lambda x: x ** 2 + 5 * x + 4)(-4))
