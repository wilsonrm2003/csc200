# question 1
nums = {1, 2, 3, 4, 5, 6}
nums = {0, 1, 2, 3} & nums
nums = filter(lambda x: x > 1, nums)
print(len(list(nums)))

# question 2
def power(x, y):
  if y == 0:
    return 1
  else:
    return x * power(x, y-1)
		
print(power(2, 3))

# question 3 
a = (lambda x: x * (x+1))(6) 
print(a)

# question 4
nums = [1, 2, 8, 3, 7]
res = list(filter(lambda x: x % 2 == 0, nums))
print(res)

# question 5 
# Drag and drop from the options below to print only the items in the set "a" that are not in the set "b".
a = {1,2,3,4}
b = {1,3,5,7}
print(a - b)
